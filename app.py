from flask import Flask
import json
import datetime
import tempfile

app = Flask(__name__)
WATER_GLASS = 10

def read_water():
    water = 0
    with open('./water.json', 'r') as f:
        data = f.read()
        print(data)
        water = json.loads(data)
    return water

def read_water_by_user(user_id):
    water = 0
    with open(f'./water{user_id}.json', 'r') as f:
        data = f.read()
        water = json.loads(data)
    return water

def save_water(water):
    with open('./water.json', 'w') as f:
        f.write(json.dumps(water))

def save_water_by_user(water, user_id):
    with open(f'./water{user_id}.json', 'w') as f:
        f.write(json.dumps(water))

# Ajoute de l'eau
@app.route('/add_water', methods=['GET'])
def add_water():
    water = read_water()
    print(water)
    water["water"] += WATER_GLASS
    if "adding" not in water.keys():
        water["adding"] = [{'added_at': str(datetime.datetime.now()), 'quantity': WATER_GLASS}]
        save_water(water)
    else:
        water["adding"].append({'added_at': str(datetime.datetime.now()), 'quantity': WATER_GLASS})
        save_water(water)
    return water



# Get water
@app.route('/water', methods=['GET'])
def water():
    filename = tempfile.TemporaryFile().name
    logfile = open(filename, 'a')
    logfile.write(f'getting water at { str(datetime.datetime.now())}')
    return read_water()


@app.route('/add_water/<user_id>')
def add_water_user(user_id):
    water = read_water_by_user(user_id=user_id)
    print(water)
    water["water"] += WATER_GLASS
    save_water_by_user(water, user_id)
    return water

if __name__ != '__main__':
    print('using as import')
else:
    app.run(debug=True)




