import pytest
import json

from app import app

@pytest.fixture()
def defapp():
    app.config.update({
        "TESTING": True,
    })
    yield app


@pytest.fixture()
def client(defapp):
    return defapp.test_client()

def dummy_data():
    print("set test")
    with open("test_data.json") as file:
        data = json.load(file)
    with open('./water.json', 'w') as f:
        f.write(json.dumps(data))
    with open('./water1.json', 'w') as f:
        f.write(json.dumps(data))

def test_request_example(client):
    response = client.get("/water")
    result = json.loads(response.data)
    print(result['water'])
    assert 70 == result['water']

def test_get_exemple(client):
    response = client.get("/add_water/1" )
    result = json.loads(response.data)
    assert 80 == result['water']

def test_add_water_without_id(client):
    response = client.get("/add_water" )
    result = json.loads(response.data)
    assert 80 == result['water']

dummy_data()